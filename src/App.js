import React, { Component } from 'react';
import { Route } from 'react-router-dom';

import logo from './logo.svg';
import './App.css';

import HomePage from './views/HomePage';
import ProfilePage from './views/ProfilePage';
import Navigation from './components/Navigator';


class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
            Hello WeManConnect
        </header>
        <Navigation />
        <main>
          <Route path="/" exact component={HomePage}/>
          <Route path="/profile" component={ProfilePage}/>
        </main>
      </div>
    );
  }
}

export default App;
