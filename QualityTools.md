
# Guide to use Quality Tools Examples

Before starting to read, just take in account that all the dependencies are already included on package.json
for this project, which means all the dependencies are automatically installed
when you run `yarn install` or `npm install`.
 
For studying purposes, instructions about how to install these dependencies in another projects are included in this guide.

## Prettier

Prettier is an opinionated code formatter.

It enforces a consistent style by parsing your code and re-printing it with its own rules that take the maximum
line length into account, wrapping code when necessary.

#### Installation

```
yarn add prettier --dev --exact
```
or
```
npm install --save-dev --save-exact prettier
```

Prettier documentation recommends pinning an exact version of prettier in your package.json 
as they introduce stylistic changes in patch releases.

#### How to Run

Without any flag, the output is the on-the-fly correction of the files.
```
prettier "filename.js"
prettier "src/**/*.js"
```

Using `--check` flag (or `-c`), will executed a verification but not rewrite your files.

```
prettier "filename.js" --check
prettier "src/**/*.js" --check
```

Using `--write` flag will executed a verification and rewrite your files with proper corrections.

```
prettier "filename.js" --write
prettier "src/**/*.js" --write
```

Using a configuration file:
```
prettier "filename.js" --config config/.prettierrc
```

#### More Documentation

You can read more about prettier.io in the following links:

[Prettier Documentation](https://prettier.io/docs/en/index.html)

[Prettier github repository](https://github.com/prettier/prettier)


## ESLint 

Code linting is a way to increase code quality. Linters as ESLint, JSHint, JSLint can detect
potential bugs, as well as code that is difficult to maintain.

JavaScript linters parse and inspect source code, and then analyze the code's syntax and structure. If any code
violates the rules defined by the linter a warning is shown. The warning explains what part of the code might lead to unexpected behavior and why.

ESLint is a tool for identifying and reporting on patterns found in ECMAScript/JavaScript code, with the goal of making code 
more consistent and avoiding bugs.

#### Installation

```
npm install eslint --save-dev
```

For supporting React, you also have to install:
```
npm install eslint-plugin-react --save-dev
npm install babel-eslint --save-dev
```

#### How to Run

You should setup a configuration file. For this demo, we are going to use a configuration file called: `.eslintrc.json`

For detailed information about configuration options, see: [Configuring ESLint](https://eslint.org/docs/user-guide/configuring)

```
./node_modules/.bin/eslint "src/App.js" --config "config/.eslintrc.json"
```
or
```
npx eslint "src/App.js" --config "config/.eslintrc.json"
npx eslint "src/**/*.js" --config "config/.eslintrc.json" 
```

For disabling validation on a file, just use this on the top of the content:
```
/* eslint-disable */
```


#### More Documentation

[ESLint Documentation](https://eslint.org/docs/user-guide/getting-started)

[ESLint github repository](https://github.com/eslint/eslint)

[ESLint for React](https://github.com/yannickcr/eslint-plugin-react)

## Some Bonus Material

[Prettier vs ESLint: What’s The Difference?](https://www.futurehosting.com/blog/prettier-vs-eslint-whats-the-difference/)

[Take a look to Prettier Playground!](https://prettier.io/playground/)

[Try to Setup Styelint!](https://stylelint.io/)